import React, { useEffect, useState } from 'react';
import setMap from './setMap';
import Drawer from './Drawer';
import axios from 'axios';
import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import List from '@material-ui/core/List';
import { makeStyles } from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Chip from '@material-ui/core/Chip';

const useStyle = makeStyles({
  buttons: {
    width: 'inherit',
    //display: 'flex',
    justifyContent: 'space-between',
    margin: 10,
    //marginBottom: 40,
    //position: 'absolute',
    //left: 0,
    //top:0,
    //zIndex:1111000,
  },
  buttonFile: {
    display: 'none',
  },
  submitButton: {
    backgroundColor: 'white',
    boxShadow: 'none',
    border: 'none',
  },
});

const getCords = async (mark) => {
  try {
    return await axios.get(
      `/site/read?XMaxDistance=500&YMaxDistance=500&x=${mark[0]}&y=${mark[1]}`
    );
  } catch {}
};

const getTime = async () => {
  try {
    return await axios.get('/time');
  } catch {}
};

const App = () => {
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const handleTime = async () => {
    const { data } = await getTime();
    setTime(data.time);
  };

  const setSnackbar = () => {
    handleTime();
    setOpenSnackbar(true);
  };

  const [openDrawer, setOpenDrawer] = useState(false);

  useEffect(() => {
    // getCords();
    setMap({
      getCords,
      setSnackbar,
      setOpenDrawer: () => {
        setOpenDrawer(true);
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const [time, setTime] = useState(null);


  const initialState = { };
  const [state, setState] = useState(initialState);
  const classes = useStyle();
  const [files, setFiles] = useState([]);
  const [isOk, setIsOk] = useState(false);
  const [isError, setIsError] = useState(false);

  const handleUploadFiles = (e) => {
    var uploadedfiles = e.target.files || e.dataTransfer.files;
    if (!uploadedfiles.length) return;

    const ArrayFiles = Array.from(uploadedfiles);
    setFiles([
      ...ArrayFiles.map((file, index) => ({
        id: `f${(~~(Math.random()*1e8)).toString(16)}`,
        name: file.name,
        file: uploadedfiles[index],
      })),
      ...files,
    ]);
  };

  const handleDeleteFile = (fileId) => () => {
    setFiles(files.filter(({ id }) => id !== fileId));
  };

  const remove = () => {
    setFiles([]);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const formdata = new FormData();

    for (const name in state) {
      formdata.append(name, state[name]);
    }

    files.forEach(({ file }) => {
      formdata.append('file', file);
    });

    axios
      .post('http://46.151.156.35:8090/map-service/site/parse-excel', formdata, {
        headers: {
          'Content-Type': 'multipart/form-data',
          'Access-Control-Allow-Credentials': true,
        },
      })
      .then((response) => {})
      .catch(({ response }) => {
        console.log(response);
        // eslint-disable-next-line no-console
        if (response && response.status === 400) {
          setIsOk(false);
          setIsError(true);
          return;
        }
        remove();
        setIsError(false);
        setIsOk(true);
      });
  };


  return (
    <div>
      <form onSubmit={handleSubmit}>
        {Boolean(files.length) && (
          <List>
            {files.map((file, id) => (
              <ListItem key={file.id}>
                <ListItemText primary={file.name} />
                <ListItemSecondaryAction>
                  <IconButton onClick={handleDeleteFile(file.id)} edge="end" aria-label="delete">
                    <DeleteIcon />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            ))}
          </List>
        )}
        <div className={classes.buttons}>
          <input
            onChange={handleUploadFiles}
            name="files"
            className={classes.buttonFile}
            type="file"
            id="file-elem"
            multiple
          />
          <label htmlFor="file-elem">
            <Chip onClick={() => {}} variant="outlined" label="Добавить файл" />
          </label>
          <button className={classes.submitButton} type="submit">
            <Chip type="submit" onClick={() => {}} color="primary" label="Отправить" />
          </button>
        </div>
      </form>
      <Drawer
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
        data={{
          title: 'ТТК раз-ка с ул. Вавилова',
          carriageway: 23442,
          sidewalks: 23423,
          roadside: 23423,
          startRenovation: 'внешнее кольцо 103  км ',
          endRenovation: ' 104 км, 3-4 ряд',
          county: 'КМ',
          basisInclusion:
            'Объекты, включенные на основании  транспортно- эксплуатационного состояния (ГБУ АД)',
          program: 'текущий ремонт',
          category: '6 категория',
        }}
      />
      <Snackbar
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={() => {
          setOpenSnackbar(false);
          setTime(null);
        }}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert
          onClose={() => {
            setOpenSnackbar(false);
            setTime(null);
          }}
        >
          Примерное время прохода {Math.round(time)} минут
        </Alert>
      </Snackbar>
    </div>
  );
};

export default App;
