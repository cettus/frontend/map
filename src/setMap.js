// eslint-disable-next-line import/no-anonymous-default-export
export default ({ data, getCords, setSnackbar, setOpenDrawer }) => {
  // eslint-disable-next-line no-undef
  var Editor = egip.layers.Editor;
  // eslint-disable-next-line no-undef
  var map = egip.layers.createMap({
    target: 'map',
    // eslint-disable-next-line no-undef
    layers: [egip.layers.createTiles2GIS()],
    // eslint-disable-next-line no-undef
    view: egip.layers.createView77({
      zoom: 11,
    }),
  });
  var multiPolygonCoordinates = [];
  var multiLineStringCoordinates = window.constants?.multiLineStringCoordinates || [];
  var multiPointCoordinates = window.constants?.multiPointCoordinates || [];
  // eslint-disable-next-line no-undef
  var multiPolygon = new egip.layers.Feature(
    // eslint-disable-next-line no-undef
    new egip.layers.geom.MultiPolygon(multiPolygonCoordinates)
  );
  // eslint-disable-next-line no-undef
  var multiLineString = new egip.layers.Feature(
    // eslint-disable-next-line no-undef
    new egip.layers.geom.MultiLineString(multiLineStringCoordinates)
  );
  // eslint-disable-next-line no-undef
  var multiPoint = new egip.layers.Feature(new egip.layers.geom.MultiPoint(multiPointCoordinates));
  // eslint-disable-next-line no-undef
  var source = egip.layers.createVectorSource({
    features: [multiPolygon],
  });
  // eslint-disable-next-line no-undef
  var layer = egip.layers.createVectorLayer({
    id: 'id',
    type: 'random',
    source: source,
    // eslint-disable-next-line no-undef
    style: new egip.layers.style.Style({
      // eslint-disable-next-line no-undef
      fill: new egip.layers.style.Fill({
        color: 'rgba(255, 255, 255, 0.2)',
      }),
      // eslint-disable-next-line no-undef
      stroke: new egip.layers.style.Stroke({
        color: 'red',
        width: 2,
      }),
      // eslint-disable-next-line no-undef
      image: new egip.layers.style.Circle({
        radius: 7,
        // eslint-disable-next-line no-undef
        fill: new egip.layers.style.Fill({
          color: 'red',
        }),
      }),
    }),
  });
  map.addLayer(layer);

  var editor = new Editor(map, layer, { oneMultiObject: false, continuousDrawing: true });

  const setNewLayer = async (mark) => {
    const {
      data: { content },
    } = await getCords(mark);
    if (content) {
      content.forEach(({ name, geometry: { coordinates } }) => {
        // eslint-disable-next-line no-undef
        var polygons = egip.layers.createVectorLayer({
          id: name,
          // eslint-disable-next-line no-undef
          source: egip.layers.createVectorSource({
            // eslint-disable-next-line no-undef
            features: [
              // eslint-disable-next-line no-undef
              new egip.layers.Feature(
                // eslint-disable-next-line no-undef
                new egip.layers.geom.MultiPolygon([[coordinates.map(({ x, y }) => [x, y])]])
              ),
            ],
          }),
          // eslint-disable-next-line no-undef
          style: new ol.style.Style({
            // eslint-disable-next-line no-undef
            stroke: new ol.style.Stroke({
              color: 'green',
              width: 8,
            }),
            // eslint-disable-next-line no-undef
            fill: new ol.style.Fill({
              color: 'rgba(0, 0, 255, 0.2)',
            }),
          }),
        });
        map.addLayer(polygons);
      });
      // eslint-disable-next-line no-undef
      var point = egip.layers.createVectorLayer({
        id: mark[0],
        // eslint-disable-next-line no-undef
        source: egip.layers.createVectorSource({
          // eslint-disable-next-line no-undef
          features: [
            // eslint-disable-next-line no-undef
            new egip.layers.Feature(
              // eslint-disable-next-line no-undef
              new egip.layers.geom.MultiPoint([mark])
            ),
          ],
        }),
        // eslint-disable-next-line no-undef
        style: new ol.style.Style({
          // eslint-disable-next-line no-undef
          stroke: new ol.style.Stroke({
            color: 'red',
            width: 8,
          }),
          // eslint-disable-next-line no-undef
          fill: new ol.style.Fill({
            color: 'rgba(0, 0, 255, 0.2)',
          }),
        }),
      });

      map.addLayer(point);
      setSnackbar();
    }

    editor.setActiveTool('Point', true);
    console.log(editor.getActiveTool());
  };

  var events = ['point_drawend'];
  events.forEach((event) =>
    editor.on(event, (e) => {
      setNewLayer(e.feature['values_'].geometry.flatCoordinates);
      console.log(event.feature?.getGeometry() + ' event from EXAMPLE:', e);
    })
  );

  editor.on('modifystart', (e) => {
    console.log('!');
  });

  var multievents = [
    'multipoint_drawstart',
    'multipoint_drawend',
    'multilinestring_drawstart',
    'multilinestring_drawend',
    'multipolygon_drawstart',
    'multipolygon_drawend',
  ];
  multievents.forEach((event) =>
    editor.on(event, (e) => {
      console.log(event.feature?.getGeometry() + ' from EXAMPLE:', e);
    })
  );

  editor.on('select', (event) => {
    setOpenDrawer();
    console.log('!');
  });

  editor.setActiveTool('Point', true);

  let point = true;
  document.onkeydown = (e) => {
    point = !point;
    console.log(point, e.keyCode);
    console.log(editor.getActiveTool());
    if (e.keyCode === 16) {
      editor.setActiveTool('Select', !point);
      editor.setActiveTool('Point', point);
    }
  };
};
