import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import ImageGallery from 'react-image-gallery';
import MUDrawer from '@material-ui/core/Drawer';
import 'react-image-gallery/styles/css/image-gallery.css';
import imgs from './imgs';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 800,
    overflowY: 'scroll',
    height: '100%',
  },
  title: {
    padding: 16,
  },
  gallery: {
    maxHeight: 500,
  },
  card: {
    margin: '24px 16px 8px 16px',
    paddingBottom: 16,
  },
  image: {
    width: 400,
    height: 400,
  },
  thumbnail: {
    width: 90,
    height: 70,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: 32,
  },
  button: {
    marginRight: 16,
  },
}));

const Drawer = ({ open, onClose, data }) => {
  const classes = useStyles();
  return (
    <MUDrawer
      variant="permanent"
      classes={{ paper: open && classes.root }}
      anchor="right"
      open={open}
      onClose={onClose}
    >
      {open && (
        <>
          <div className={classes.title}>
            <Typography variant="h5">{data.title}</Typography>
          </div>
          <div>
            <Card className={classes.card}>
              <CardHeader title="Фотоотчет" />
              <CardContent className={classes.gallery}>
                <ImageGallery
                  showPlayButton={false}
                  showFullscreenButton={false}
                  items={imgs.map((img) => ({
                    original: img,
                    sizes: '100x100',
                    renderItem: () => <img alt="item" className={classes.image} src={img} />,
                    renderThumbInner: () => (
                      <img alt="thumb" className={classes.thumbnail} src={img} />
                    ),
                  }))}
                />
              </CardContent>
            </Card>
            <Card className={classes.card}>
              <CardHeader title="Начальная" />
              <CardContent>
                <Typography variant="h6">{data.startRenovation}</Typography>
              </CardContent>
            </Card>
            <Card className={classes.card}>
              <CardHeader title="Конечная" />
              <CardContent>
                <Typography variant="h6">{data.endRenovation}</Typography>
              </CardContent>
            </Card>
            <Card className={classes.card}>
              <CardHeader title="Округ" />
              <CardContent>
                <Typography variant="h6">{data.county}</Typography>
              </CardContent>
            </Card>
            <Card className={classes.card}>
              <CardHeader title="Основание для включения" />
              <CardContent>
                <Typography variant="h6">{data.basisInclusion}</Typography>
              </CardContent>
            </Card>
            <Card className={classes.card}>
              <CardHeader title="Программа" />
              <CardContent>
                <Typography variant="h6">{data.program}</Typography>
              </CardContent>
            </Card>
            <Card className={classes.card}>
              <CardHeader title="Категория объекта" />
              <CardContent>
                <Typography variant="h6">{data.category}</Typography>
              </CardContent>
            </Card>

            <Card className={classes.card}>
              <CardHeader title="Проезжая часть" />
              <CardContent>
                <Typography variant="h6">{data.carriageway} площадь, кв.м.</Typography>
              </CardContent>
            </Card>
            <Card className={classes.card}>
              <CardHeader title="Тротуары" />
              <CardContent>
                <Typography variant="h6">{data.sidewalks} площадь, кв.м.</Typography>
              </CardContent>
            </Card>
            <Card className={classes.card}>
              <CardHeader title="Обочины" />
              <CardContent>
                <Typography variant="h6">{data.roadside} площадь, кв.м.</Typography>
              </CardContent>
            </Card>
          </div>
          <div className={classes.buttons}>
            <Button className={classes.button} variant="contained" color="primary">
              Закрыть инспекцию
            </Button>
            <Button variant="contained" onClick={onClose} color="secondary">
              Закрыть окно
            </Button>
          </div>
        </>
      )}
    </MUDrawer>
  );
};

export default Drawer;
